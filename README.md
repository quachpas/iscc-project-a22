# ISCC - Paper Presentation

## Paper

Xu, P., Davoine, F., Bordes, JB. et al. Multimodal information fusion for urban scene understanding. Machine Vision and Applications 27, 331–349 (2016). [https://doi.org/10.1007/s00138-014-0649-7](https://doi.org/10.1007/s00138-014-0649-7)


## Group members

- Maxime Delboulle
- Noé Amiot
- Pascal Quach

## Requirements

Either working TeX Live distribution, or [Tectonic](https://tectonic-typesetting.github.io/install.html)

## Build instructions

### Tectonic

```shell
tectonic -X compile presentation.tex 
```

### TeX Live

```shell
latex presentation.tex
biber presentation
latex presentation.tex
latex presentation.tex
dvipdf presentation.dvi
```

or 

```shell
make
```

### Cleaning

```shell
make clean
```





