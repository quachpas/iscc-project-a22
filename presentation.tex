\documentclass[9pt]{beamer}
\usetheme{Frankfurt}
\usepackage[]{csquotes} 
\usepackage[backend=biber, url=false, eprint=false, isbn=false]{biblatex} 
\bibstyle{authoryear-comp}
\addbibresource{bibliography.bib}
\usepackage[]{graphicx} 
\usepackage[]{subcaption} 
\graphicspath{{figures/}}
\usepackage[normalem]{ulem} 
\usepackage[]{xcolor}
\usepackage[]{xfrac}

% Title page details
\title{ISCC - Paper presentation}
\subtitle{Multimodal Information Fusion for Urban Scene Understanding} 
\author{Noé Amiot, Maxime Delboulle, Pascal Quach}
\institute{Université de Technologie de Compiègne}
\date{\today}
\setbeamertemplate{footline}[frame number]
\titlegraphic{
	\includegraphics[width=0.25\linewidth]{utc}    
}

% Document
\begin{document}
\begin{frame}[t]
	\titlepage
\end{frame}

\begin{frame}[t]
	\frametitle{Outline}
	\setcounter{tocdepth}{1}
	\tableofcontents[]
\end{frame}

\section{Introduction}
\subsection{Problem definition}
\begin{frame}[t]
	\frametitle{Problem definition}

	The paper focuses on the problem of \emph{scene understanding}, applied to
	Advanced Driver Assistance Systems.

	\emph{Scene understanding} can be divided into many challenging
	subtasks, e.g., road recognition, obstacle detection, pedestrian
	detection. Many algorithms have been developed to solve these problems
	\emph{individually}.

	The paper proposes a method to fuse different sources of information,
	such that the two main difficulties of \textbf{information fusion} are solved:
	
	\begin{enumerate}
		\item Fuse modules with different classes, and be able to add
			previously unknown classes
		\item Represent the sensors' output in a common space
	\end{enumerate}
	
	\begin{block}{Unimodal scene understanding}
		LiDARs are commonly used to detect
		static~\cite{thrun.etal_2005} and
		moving~\cite{wang.etal_sep2007} structures.
		
		Cameras are applied to a much wider range of tasks, e.g.,
		pedestrian detection~\cite{dollar.etal_apr2012}, obstacle
		detection~\cite{badino.etal_2007}
	\end{block}
\end{frame}
	
\subsection{Related works}%
\begin{frame}[t]
	\frametitle{Related works}
	% example \cite{xu.etal_apr2016} 
	\begin{block}{Multimodal scene understanding fusion techniques}
		\begin{itemize}
			\item Region of interest (ROI)
				approach~\cite{rodriguezflorez.etal_apr2014}
			\item Geometric approach by inferring object detection
				constraints from the ground
				plane~\cite{leibe.etal_jun2007}
			\item Other approaches include feature combination
		\end{itemize}
	\end{block}

	\begin{alertblock}{Issues}
	These approaches have caveats:
	\begin{enumerate}
		\item They are specialized to solve one task
		\item Their output cannot be directly combined with other
			information modalities
		\item In order to add new classes of objects, the models have
			to be retrained from scratch
	\end{enumerate}
	\end{alertblock}
\end{frame}

\subsection{Contributions}%
\begin{frame}[t]
	\frametitle{Contributions}

	The authors present an original framework for multimodal information
	fusion based on Dempster-Shafer theory and over-segmented images. 

	The framework is:
	\begin{itemize}
		\item \textbf{flexible}, new classes/sensors/algorithms can
			be introduced without retraining the whole system
		\item \textbf{data efficient}, modules process data
			independently and in parallel
		\item \textbf{robust} to sensor failure, as modularity
			increases the system's resilience
	\end{itemize}
\end{frame}

\section[Problem formulation]{Problem formulation: scene understanding as image segment labelling}
\begin{frame}[t]
	\frametitle{Image segment labelling}
	\begin{figure}[htpb]
		\captionsetup[subfigure]{justification=centering}
		\centering
		\begin{subfigure}[t]{0.45\textwidth}
			\begin{center}
				\includegraphics[width=0.9\linewidth]{photo_scene_oversegmentation_SLIC.png}
			\end{center}
			\caption{Over-segmentation from the SLIC algorithm}
			\label{fig:slic-over-segmentation}
		\end{subfigure}
		\begin{subfigure}[t]{0.45\textwidth}
			\begin{center}
				\includegraphics[width=0.9\linewidth]{photo_scene_classification_output.jpg}
			\end{center}
			\caption{Image segments classification example}
			\label{fig:image-segments-classified}
		\end{subfigure}

		\caption{Image segment classification problem}%
		\label{fig:image-segment-classification}
	\end{figure}

	For ADAS, reflecting what the driver perceives can be seen as labelling
	an image. As pixel level labelling may be too local and difficult, the
	authors choose an intermediate granularity based on over-segmentation,
	using the SLIC algorithm~\footnote{Simple Linear Iterative
	Clustering~\cite{achanta.etal_nov2012}}.
\end{frame}

\section[Reliability and decision-making]{Reliability measure and decision-making strategy}
% TODO: explain why we talk about reliability measure + decision-making
% or, simply a better transition

\subsection{Reliability measure} % TODO
\begin{frame}[t]
	\frametitle{Discounting a mass function}
	
	\begin{block}{Discounting factor}
		Information reliability can be quantified using a
		\textbf{discounting factor}, weakening a mass function by
		transferring some of its mass to the ignorance state.
	\end{block}
	
	Given a discounting factor $\alpha$ in $[0,1]$, the discounted mass
	function $^{\alpha}m$ is given by:

	\begin{align*}
		^{\alpha}m(A) &= (1 - \alpha)m(A),\quad \forall A \not\subseteq \Omega\\
		^{\alpha}m(\Omega) &= (1 - \alpha)m(\Omega) + \alpha
	\end{align*}
	
	% TODO: is it really needed?
	%With $\beta$ = 1 - $\alpha$, we can rewrite:
	
	%$$^{\beta}m(A) = \beta \times m(A), \forall A \not\subseteq \Omega $$
	%$$^{\beta}m(\Omega) = 1 - \beta (1 - m(\Omega)) = 1 - \beta \times \sum_{A \not\subseteq \Omega}^{} m(A) = 1 - \sum_{A \not\subseteq \Omega}^{} (^{\beta}m(A)) $$

	%$\rightarrow$ \textbf{proportional loss} $\forall A$ that contributes to total ignorance $\Omega$

	%\emph{This approach is general and will be applied to many modules}

\end{frame}

\subsection{Decision-making strategies} % TODO
\begin{frame}[t]
	\frametitle{Optimistic strategy incentives}

	\begin{table}[!ht]
		\centering
		\begin{tabular}{|l|l|l|l|l|}
		\hline
			\textbf{Name} & \textbf{Strategy} & \textbf{Complexity} & \textbf{Tendency} & \textbf{Coherence} \\ \hline
			pessimistic & max Bel & $\mathcal{O}(|\Omega|^{k})$ & No choice & No \\ \hline
			\color{red} optimistic & max Pl & $\mathcal{O}(k|\Omega|)$ & choice & Yes \\ \hline
			pegnistic & max betP & $\mathcal{O}(e^{|\Omega|})$ & choice & No \\ \hline
		\end{tabular}
	\end{table}

	\begin{block}{Adopted strategy}

		The authors adopt the \textbf{optimistic} strategy, due to multiple reasons:
		
		\begin{itemize}
			\item it has \textbf{low computational complexity}: 
			\begin{enumerate}
				\item requirement for real-time applications
				\item can add more sensors reasonably without
					high costs
			\end{enumerate}
			\item it is \textbf{coherent}: identical
				decision-making for all granularities of
				$\Omega$
		\end{itemize}
	
	\end{block}

\end{frame}

\section[Multimodal system]{Multimodal sensor system}

\subsection{System inputs}%
\begin{frame}[t]
	\frametitle{Multisensor system inputs}
	\framesubtitle{Over-segmentation, laser points, disparity map, optical flow}
	% SLIC, Velodyne LiDAR, ELAS, TV-L^1

	The authors make use of a stereo camera, and a LiDAR sensor, which are
	assumed calibrated. They derive the following inputs for their system:

	\begin{itemize}
		\item over-segmented image, computed using the SLIC
			algorithm~\cite{achanta.etal_nov2012}
		\item a single laser layer from a Velodyne LiDAR
		\item disparity map, computed using the ELAS\footnote{Efficient Large Scale Stereo Matching}
			algorithm~\cite{geiger.etal_2011}
		\item optical flow, computed using the TV-L1\footnote{Total
				Variation regularization and $L^1$ norm in the
			data fidelity term}
	\end{itemize}

	\begin{figure}[htpb]
		\centering
		\begin{subfigure}[b]{0.48\textwidth}
		\begin{center}
			\includegraphics[width=\linewidth]{oversegmentation_input.png}
		\end{center}
		\caption{Over-segmentation}
		\label{fig:over-segmented}
		\end{subfigure}
		\begin{subfigure}[b]{0.48\textwidth}
		\begin{center}
			\includegraphics[width=\linewidth]{disparity_map_input.png}
		\end{center}
		\caption{Disparity map}
		\label{fig:disparity-map}
		\end{subfigure}
		
		\begin{subfigure}[b]{0.48\textwidth}
		\begin{center}
			\includegraphics[width=\linewidth]{laser_points_input.png}
		\end{center}
		\caption{Laser points}
		\label{fig:laser-points}
		\end{subfigure}
		\begin{subfigure}[b]{0.48\textwidth}
		\begin{center}
			\includegraphics[width=\linewidth]{optical_flow_input.png}
		\end{center}
		\caption{Optical flow}
		\label{fig:optical-flow}
		\end{subfigure}
		
		\caption{Multimodal system inputs}
		\label{fig:system-inputs}
	\end{figure}

\end{frame}

\subsection{Architecture}
\begin{frame}[t]
	\frametitle{System architecture}

	\begin{columns} % TeX Black Magic: Increase Margin
		\column{\dimexpr\paperwidth-5pt}
		\begin{figure}[htpb]
			\captionsetup[subfigure]{justification=centering}
			\captionsetup[subtable]{position=bottom}
			\centering
			
			\begin{subtable}[b]{0.32\textwidth}
				\centering
				\footnotesize
				\begin{tabular}{lp{0.55\textwidth}}
					Module & Frame of discernment \\ 
					Pixel & $\Omega_S = \{ \text{S}, \text{\sout{S}} \}$\\
					Pixel & $\Omega_G = \{ \text{G}, \text{\sout{G}} \}$\\
					Stereo & $\Omega_G = \{ \text{G}, \text{\sout{G}} \}$\\
					LiDAR & $\Omega_G = \{ \text{G}, \text{\sout{G}} \}$\\
					Surface & $\Omega_G = \{ \text{G}, \text{Vt}, \text{S} \}$\\
					Texture & $\Omega_S = \{ \text{V}, \text{\sout{V}} \}$\\
					Optical flow & Multiple\\
				\end{tabular}
				
				\caption[position=bottom]{Frames of discernment per module}
				\label{tab:modules-fod}
			\end{subtable}
			\begin{subfigure}[b]{0.67\textwidth}
				\centering
				\includegraphics[width=\linewidth]{system_flowchart.png}
				\caption{System flowchart}
				\label{fig:system-flowchart}
			\end{subfigure}
			
			\caption{System Architecture}
			\label{fig:system-architecture}
		\end{figure}
	\end{columns}

	We use the following abbreviations for classes: S for \emph{sky}, G for
	\emph{ground}, Vt for \emph{vertical}, V for \emph{vegetation}.

\end{frame}

\subsection{Classification modules}
% TODO: we can skip some of these slides as needed during the presentation

\subsubsection{Pixel-based classification}
\begin{frame}[t]
	\frametitle{Pixel-based classification I}
	\framesubtitle{Ground and sky detection}
	%Assumption pitch angle V_min/V_max, violated when e.g. uphill/downhill
	%=> robust horizon line estimator
	% Initial mass functions for frame ground/sky/"vertical"

	The authors integrate the belief that the \enquote{lower} part of the
	image cannot be the \textit{sky}, and conversely, that the \enquote{upper}
	part cannot be the \textit{ground}.

	\begin{alertblock}{Maximum pitch angle assumption}
		A maximum pitch angle of $\pm 5^\circ$ is assumed, resulting
		in the upper and lower bounds of the horizon line $V_{\max}$
		and $V_{\min}$.

		A robust horizon line estimator may be needed in situations
		where this assumption does not hold, e.g., downhill or uphill.
	\end{alertblock}

	Given two frames $\Omega_S=\{ \text{sky}, \text{\sout{sky}}\}$, and
	$\Omega_G=\{ \text{ground}, \text{\sout{ground}}\}$, and a segment's
	minimum and maximum vertical coordinate $\underline{v}$ and $\bar{v}$:

	\begin{align}
		m_{\bar{v}}(\{ \text{\sout{sky}} \}) &= \begin{cases}
			1 & \text{if }\bar{v} \leq V_{\min}\\
			0 & \text{otherwise}
		\end{cases}\\
			m_{\bar{v}}(\{ \text{sky} \}) &= 0\\
			m_{\bar{v}}(\{ \Omega_S \}) &= 1 - m_{\bar{v}}(\{ \text{\sout{sky}} \})
	\end{align}
\end{frame}

\begin{frame}[t]
	\frametitle{Pixel-based classification II}
	\framesubtitle{Ground and sky detection}
	
	Mass functions for the \textit{ground} are defined similarly, using
	$\underline{v}$ and $\Omega_G$ instead.

	Note that \textit{ignorance} ($m_{\overline{v}}(\{ \text{\sout{sky}}\}
	= 0$) is represented by a vacuous mass function, i.e., 
	\begin{equation*}
			m_{\bar{v}}(\{ \Omega_S \}) = 1
	\end{equation*}

	The combination of $m_{\bar{v}}(\{ \Omega_S \})$, and
	$m_{\underline{v}}(\{ \Omega_G \})$ on a common frame  $\Lambda =
	\{\text{sky}, \text{ground}, \text{vertical}\}$ gives:

	\begin{align}
		m_{\underline{v},\bar{v}}^{\Lambda}(\{ \overline{\text{sky}} \}) &= m_{\bar{v}}(\{ \text{\sout{sky}} \}) \\
		m_{\underline{v},\bar{v}}^{\Lambda}(\{ \overline{\text{ground}} \}) &= m_{\underline{v}}(\{ \text{\sout{ground}} \}) \\
		m^{\Lambda}(\Lambda) &= 1 - m_{\underline{v},\bar{v}}(\{ \overline{\text{sky}} \}) - m_{\underline{v},\bar{v}}(\{ \overline{\text{ground}} \})
	\end{align}
\end{frame}

\subsubsection{Stereo-based classification}%
\begin{frame}[t]
	\frametitle{Stereo-based classification I}
	\framesubtitle{Distance to the ground plane}
	
	\begin{enumerate}
		\item Estimate a disparity map using the ELAS algorithm
			~\cite{geiger.etal_2011} 
		\item Assuming a planar ground, the ground surface is estimated
			using RANSAC
	\end{enumerate}

	% Segment
	Each segment is considered as a collection of 3D points, with $k$ out
	of $n$ valid points, i.e. for which a disparity has been estimated.

	\begin{equation*}
		\mathbf{x} = \{ p_1, \dots, p_k, p^*_{k+1}, \dots, p^*_n \}
	\end{equation*}

	% Distances
	Consider for each segment, the minimum and maximum distance from the segment
	to the ground plane. 	
	\begin{equation*}
		\underline{d} = \min_{i=1,\dots,k} \delta( p_i, \Pi)\quad
		\bar{d} = \max_{i=1,\dots,k} \delta( p_i, \Pi)
	\end{equation*}

	% Figure
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.8\textwidth]{median_distance_ground_plane}
		\caption{Median distance to ground plane}
		\label{fig:median-distance-ground-plane}
	\end{figure}
\end{frame}
\begin{frame}[t]
	\frametitle{Stereo-based classification II}
	\framesubtitle{Mass functions}
	
	% Belief functions
	Given some training data $\{(d_i, y_i)\}_{1 \leq i \leq N}$, with $y_i$
	a binary variable stating whether $d_i$ is associated with the ground
	or not.  

	We define the mass functions for $\underline{d}$. The mass functions
	for $\bar{d}$ are defined similarly.

	\begin{align*}
		\underline{m}_i = m_{\underline{d}}^{\Omega_G}(\{ \text{\sout{ground}} \}) &= \frac{1}{1 + \exp(\underline{ad}) + \underline{b}} \\
		m_{\underline{d}}^{\Omega_G}(\{ \text{ground} \}) &= 0 \\
		m_{\underline{d}}^{\Omega_G}(\Omega_G) &= 1 - m_{\underline{d}}^{\Omega_G}(\{ \text{\sout{ground}} \})\\
		\max_{a,b\in \mathbb{R}} \sum_{i=1}^N y_i \log(\underline{m}_i) &+ (1 - y_i) \log ( 1 - \underline{m}_i)
	\end{align*}

	The mass function is discounted by a factor 

	\[
		\alpha = 1 - \frac{k}{n}
	.\] 

	\end{frame}
	
%\begin{frame}[t] % TODO: might not be needed
	%\frametitle{Probabilistic stereo-based classification}
	%% Probabilistic model
	%Consider for each segment the median distance of the valid points to
	%the ground plane $\Pi$.

	%\begin{equation*}
		%\text{d}( \mathbf{x}, \Pi ) = \underset{i=1,\dots,k}{\text{med}} \delta(p_i, \Pi)
	%\end{equation*}

	%\dots
%\end{frame}

\subsubsection{LiDAR-based classification}
\begin{frame}[t]
	\frametitle{LiDAR-based classification}
	\framesubtitle{Ground detection}
	% Fig 8.b
	% \Omega = {\neg ground, ground}
	% R: \neg ground, G: ground, V: \Omega_G

	% Green line
	\begin{enumerate}
		\item Draw a green line between the LiDAR impact ({\color{red}
			red} dots) and their projections on the plane estimated
			by the stereo module ({\color{green} green} dots)
		\item All segments crossed by the {\color{green} green} line
			are assimilated to the \emph{ground} class
		\item Ambiguous segments between the {\color{red} red} and
			{\color{green} green} dots ({\color{blue} blue} lines)
			are modelled by a vacuous mass function (uniform
			distribution)
		\item Mass functions are discounted by a factor $\alpha$,
			corresponding to the ratio of available disparity
			measures to the maximum number of beams that could 
			have hit the image segment.
	\end{enumerate}

	% iff hit/crossed by one laser beam

	\begin{minipage}[c]{0.48\textwidth}
	%mass function
	\begin{align*}
		m_L^{\Omega_G} (\Omega_G) &= 1\\
		m_L^{\Omega_G} (\{ ground \}) &= 1\\
		P_R (r=0) = \alpha &= \frac{k}{n}
	\end{align*}
	\end{minipage}
	% Figure
	\begin{minipage}[c]{0.48\textwidth}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.9\linewidth]{laser_segments.png}
		\caption{Laser segments}
		\label{fig:laser-segments}
	\end{figure}
		
	\end{minipage}
\end{frame}

\subsubsection{Surface-based classification}%
\begin{frame}[t]
	\frametitle{Surface-based classification}%
	\framesubtitle{Ground, sky, and vertical classification}

	The authors use pre-trained models from~\cite{hoiem.etal_oct2007}, that
	consider three classes: \textit{vertical}, \textit{ground}, and
	\textit{sky}. The output is a probability measure.

	\begin{alertblock}{Probability to mass function}
		The probabilistic output is considered as the pignistic
		probability generated by a non-Bayesian mass function, as
		considering the output as a Bayesian mass function would
		restrict further combination to this class of functions.
	\end{alertblock}
	% Fig 8.c
	% Probabilities
	% R: vertical, G: ground, B: sky
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.55\textwidth]{monocular_surface.png}
		\caption{Monocular surface layout estimation results. The RGB
			colours correspond respectively to the probability of
			\textit{vertical}, \textit{ground}, and \textit{sky}
		classes.}%
		\label{fig:monocular-surface}
	\end{figure}

	The authors use a result from~\cite{dubois.etal_jun2008}, to compute a
	pseudo-inverse, i.e., compute a mass function from a probability
	measure, then discount based on the accuracy of the algorithm.
\end{frame}

\subsubsection{Texture}
\begin{frame}[t] %
	\frametitle{Texture-based classification}
	\framesubtitle{Vegetation}
	% Fig 8.d
	% \Omega_V = {\neg vegetation, vegetation}
	% R: \neg vegetation, G: vegetation, B: \Omega_V

	% Procedure
	% Walsh-Hadamard transform to encode the texture
	% Train vegetation binary classifier using L1-regularized logistic regression
	% Same procedure to go from probability to mass functions as in surface-based classification.
	% No discounting, trained directly on KITTI

	To classify image segments based on texture, the authors proceed as such:

	\begin{enumerate}
		\item The texture is encoded using the Walsh-Hadamard
			transform~\cite{wojek.schiele_2008}
		\item A binary classifier for vegetation is trained using
			$L^1$-regularized logistic regression
		\item Mass functions are obtained using the same procedure as
			for the surface layout module
	\end{enumerate}

	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.8\textwidth]{vegetation_detection.png}
		\caption{Vegetation detection using texture information. The
			RGB colours represent the mass assigned to classes
			\{\sout{vegetation}\}, \{vegetation\}, and $\Omega_V$
		respectively.}%
		\label{fig:vegetation-detection}
	\end{figure}
\end{frame}

\subsection{Temporal information propagation}%
\begin{frame}[t]
	\frametitle{Optical flow-based information propagation}
	
	The authors use an optical flow-based temporal propagation, such that:
	\begin{enumerate}
		\item The optical flow is computed using the TV-L1
			algorithm~\cite{zach.etal_2007}
		%TODO: it's a time machine! Check if its correct
		\item Each segment $S_t$ is associated with a previous segment
			$S_{t-1}$, such that $S_{t-1}$ is the segment pointed
			by the mean flow of the pixels in $S_t$
		\item Mass functions, or probability are propagated from $S_{t-1}$
			to $S_t$ following the associations
		\item The propagation is discounted by a factor corresponding
			to the ratio of pixels in $S_t$ whose flow points to
			$S_{t-1}$.
	\end{enumerate}

	\begin{figure}[htpb]
		\centering
		\begin{subfigure}[b]{0.48\textwidth}
		\begin{center}
			\includegraphics[width=\linewidth]{temporal_prop_t-1.png}
		\end{center}
		\caption{Detection at time $t-1$}
		\label{fig:detection-t-1}
		\end{subfigure}
		\begin{subfigure}[b]{0.48\textwidth}
		\begin{center}
			\includegraphics[width=\linewidth]{temporal_prop_t.png}
		\end{center}
		\caption{Propagated detection at time $t$}
		\label{fig:detection-t-1}
		\end{subfigure}
		
		\caption{Temporal propagation for the stereo-based ground detection}
		\label{fig:temporal-propagation}
	\end{figure}
\end{frame}

\section[Evaluation]{Evaluation on urban driving scene data (KITTI)}
\begin{frame}[t] 
	\frametitle{Multimodal system evaluation I}
	\framesubtitle{KITTI dataset}
	
	The system was evaluated on the KITTI Dataset~\cite{geiger.etal_sep2013}.

	110 images were manually annotated, selected to depict a high variety
	of scene, with 70 images for training, and 40 for testing.

	\begin{block}{Training}
		The training set is used to estimate:
		\begin{itemize}
			\item parameters \& probabilities of stereo, LiDAR and
				texture-based modules
			\item discounting factor of monocular surface layout estimation
		\end{itemize}
		
	\end{block}
	
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.9\textwidth]{probabilistic_evidential_ground_truth_labelling.png}
		\caption{Qualitative results of the combination of all the
			modules. From left to right, probabilistic, evidential,
			and ground truth. The color code is the following:
			\textit{grass} is {\color{magenta} magenta},
			\textit{road} is {\color{green} green}, \textit{tree}
			is {\color{yellow} yellow}, \textit{obstacle} is
			{\color{red} red}, and \textit{sky} is {\color{blue}
		blue}}
	\end{figure}
\end{frame}
\begin{frame}[t]
	\frametitle{Multimodal system evaluation II}
	\framesubtitle{KITTI dataset}

	\begin{block}{Recall and error rates for probabilistic and evidential fusion}
		\begin{enumerate}
			\item ground detection: similar good results, modules
				are complementary
			\item addition of sky and vegetation detection: over-confidence
				on ground \& sky for probabilistic fusion as the
				granularity of non-ground is greater.
		\end{enumerate}
			
		Evidential fusion is more robust and accurate.
	\end{block}

	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.9\textwidth]{evaluation_kitti.png}
		\caption{Quantitative results of the combination of all the modules}
	\end{figure}
\end{frame}

\section{Conclusion}
\begin{frame}[t]
	\frametitle{Conclusion}

	\begin{block}{Advantages}
		The presented system is:

		\begin{itemize}
			\item more accurate, and easily extendable with new classes
			\item more robust than probabilistic fusion when
				extended
			\item fairly computationally simple, with parallel
				computation per module
		\end{itemize}
	\end{block}

	\begin{alertblock}{Drawbacks}
		However, it presents 
		\begin{itemize}
			\item The assumption that some probability outputs are
				pignistic probabilities generate by
				non-Bayesian mass functions is a loss of
				information
			\item The flexibility of the system does not allow
				deriving global learning, or optimized
				combination rules.
		\end{itemize}
	\end{alertblock}

	\textbf{Further Work} 
	Additional classes (e.g., pedestrian), and new sources of information to
	detect moving objects (e.g. GPS, digital maps) will be considered.
	Deeper understanding will be further studied using methods to merge
	segments belonging to the same object.
\end{frame}

\section{References}
\begin{frame}[t, allowframebreaks]
	\frametitle{References}
	\printbibliography
\end{frame}
	
\end{document}
